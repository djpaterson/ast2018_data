% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/statistics.R
\name{stats_significance_table}
\alias{stats_significance_table}
\title{FUNCTION stats_significance_table
Produces a table showing the significance values of varying numbers of data frames by
a specified measure. For example, this function can compare significance of different
fault types (real vs mutant) or numbers of faults (1, 5 and 10).}
\usage{
stats_significance_table(dfs = list(), col.headers = c(), ...)
}
\description{
FUNCTION stats_significance_table
Produces a table showing the significance values of varying numbers of data frames by
a specified measure. For example, this function can compare significance of different
fault types (real vs mutant) or numbers of faults (1, 5 and 10).
}
\examples{
stats_significance_table(dfs=list(results.1realfault, results.1mutant), col.headers=c("Real", "Mutant"))
stats_significance_table(dfs=list(results.1realfault, results.5realfault, results.10realfault), col.headers=c("1", "5", "10"))
}
