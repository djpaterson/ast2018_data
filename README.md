# ast2018_data #

This repository contains the data and code required to replicate the plots and tables presented in our submitted AST2018 paper. All of the code
in this repository is in R, tested on version 3.4.2

### Installation Instructions ###
* Clone this repository
```
git clone https://bitbucket.org/djpaterson/ast2018_data.git /your/path/here/
```
* Run R and install devtools
```
install.packages("devtools")
```
If the devtools installation is unsuccessful, it may be because your distribution does not have Curl installed
* Use devtools to install package
```
devtools::install_local("/your/path/here/")
```
* Load the package
```
library(testprioritisation)
```
* List the available functions and documentation
```
library(help = testprioritisation)
```
* Read results into memory (e.g.)
```
results.1realfault <- get_results("realfault", 1)
results.5realfault <- get_results("realfault", 5)
results.10realfault <- get_results("realfault", 10)
```
In most cases, we want to reduce the size of the results, since our processed data contains information about every trigger test, rather
than only the first for every fault (e.g.)
```
results.1realfault %<>% transform_reduce()
results.5realfault %<>% transform_reduce()
results.10realfault %<>% transform_reduce()
```
* Start producing plots and tables (e.g.)
```
visualise_stats_compare(dfs = list(results.1realfault, results.5realfault, results.10realfault), by = "num.faults", cname = "Num Faults")
```
For help on functions, type `?function` for documentation

### Using these functions with new data ###

During the development of this R package, initially all of the raw experimental data was included in the same package. However, this made
installation times excessive and caused a great deal of bulk. In order to avoid this, the raw data is expected to be found in a second
R package called `testprioritisation.data`. Once processed using the `get_results` function, data can be stored in rds format using `write_rds`
and is expected to be found in the `inst/extdata/rds` folder of this package.

In order to add extra data:

* Create a new R package called `testprioritisation.data`
* Inside the package, create the folders `inst` and `inst/extdata`.
* Inside `extdata`, add data in the form `<project>/<version>/<num_faults><fault_type>/<algorithm>/<repetition>/ordering/<filename>.csv`. It is worth noting that our tool [`Kanonizo`](https://www.github.com/kanonizo/kanonizo) produces output in the form `ordering/<filename>.csv` already, and can be configured to use the correct path as expected by this package
 > e.g. in our original experiments data would be inside `testprioritisation.data` as Lang/1/1realfault/greedy/1/ordering/BUGGED.csv
* The R code should work out which projects/bugs are included automatically using the files in `extdata`
* Call `get_results(fault_type, num_faults)`. This will automatically write an RDS file for future usage unless you specify write=F. You can also force a reload of the data using force_reload=T

### Who do I talk to? ###

* Contact dpaterson1@sheffield.ac.uk with any issues