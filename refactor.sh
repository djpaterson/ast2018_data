die(){
 echo $1
 exit 1
}

[ $# -eq 2 ] || die "Usage: refactor.sh <function_name> <desired_function_name>"
function_name=$1
desired_function_name=$2

ls R | while read -r file; do 
  perl -pi -e "s/$function_name/$desired_function_name/g" R/$file; 
done
